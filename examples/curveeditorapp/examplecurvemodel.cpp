/****************************************************************************
**
** Copyright (C) 2019 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Design Tooling
**
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 as published by the Free Software
** Foundation with exceptions as appearing in the file LICENSE.GPL3-EXCEPT
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
****************************************************************************/
#include "examplecurvemodel.h"
#include "animationcurve.h"
#include "curveeditorstyle.h"
#include "treeitem.h"

#include <QDebug>

namespace DesignTools {

ExampleCurveModel::ExampleCurveModel()
    : CurveEditorModel()
{
    connect(this, &CurveEditorModel::currentFrameChanged, this, &ExampleCurveModel::currentFrameChangeEmitted);
    connect(this, &CurveEditorModel::curveChanged, this, &ExampleCurveModel::curveChangeEmitted);
}

double ExampleCurveModel::minimumTime() const
{
    return 0.0;
}

double ExampleCurveModel::maximumTime() const
{
    return 500.0;
}

CurveEditorStyle ExampleCurveModel::style() const
{
    // Pseudo auto generated. See: CurveEditorStyleDialog
    CurveEditorStyle out;
    out.backgroundBrush = QBrush(QColor(55, 55, 55));
    out.backgroundAlternateBrush = QBrush(QColor(0, 0, 50));
    out.fontColor = QColor(255, 255, 255);
    out.gridColor = QColor(114, 116, 118);
    out.canvasMargin = 15;
    out.zoomInWidth = 99;
    out.zoomInHeight = 99;
    out.timeAxisHeight = 40;
    out.timeOffsetLeft = 10;
    out.timeOffsetRight = 10;
    out.rangeBarColor = QColor(46, 47, 48);
    out.rangeBarCapsColor = QColor(50, 50, 255);
    out.valueAxisWidth = 60;
    out.valueOffsetTop = 10;
    out.valueOffsetBottom = 10;
    out.handleStyle.size = 12;
    out.handleStyle.lineWidth = 1;
    out.handleStyle.color = QColor(255, 255, 255);
    out.handleStyle.selectionColor = QColor(255, 255, 255);
    out.keyframeStyle.size = 13;
    out.keyframeStyle.color = QColor(172, 210, 255);
    out.keyframeStyle.selectionColor = QColor(255, 255, 255);
    out.curveStyle.width = 1;
    out.curveStyle.color = QColor(0, 200, 0);
    out.curveStyle.selectionColor = QColor(255, 255, 255);
    return out;
}

void ExampleCurveModel::createItems()
{
    AnimationCurve curve1({
        Keyframe(QPointF(0.0, -1.0), QPointF(), QPointF(100.0, -1.0)),
        Keyframe(QPointF(150.0, 0.7), QPointF(50.0, 0.7), QPointF(250.0, 0.7)),
        Keyframe(QPointF(500.0, 1.0), QPointF(400.0, 1.0), QPointF())});

    AnimationCurve curve2({
        Keyframe(QPointF(0.0, -1.0), QPointF(), QPointF(100.0, -1.0)),
        Keyframe(QPointF(200.0, 0.4), QPointF(100.0, 0.4), QPointF(300.0, 0.4)),
        Keyframe(QPointF(500.0, 1.0), QPointF(400.0, 1.0), QPointF())});

    AnimationCurve curve3({
        Keyframe(QPointF(0.0, -1.0), QPointF(), QPointF(100.0, -1.0)),
        Keyframe(QPointF(250.0, 0.0), QPointF(150.0, 0.0), QPointF(350.0, 0.0)),
        Keyframe(QPointF(500.0, 1.0), QPointF(400.0, 1.0), QPointF())});

    AnimationCurve curve4({
        Keyframe(QPointF(0.0, -1.0), QPointF(), QPointF(100.0, -1.0)),
        Keyframe(QPointF(300.0, -0.4), QPointF(200.0, -0.4), QPointF(400.0, -0.4)),
        Keyframe(QPointF(500.0, 1.0), QPointF(400.0, 1.0), QPointF())});

    auto *instance = new NodeTreeItem("Instance", QIcon(":/ICON_INSTANCE"));
    instance->addChild(new PropertyTreeItem("Position X", curve1));
    instance->addChild(new PropertyTreeItem("Position Y", curve2));

    auto *material = new NodeTreeItem("Material", QIcon(":/ICON_MATERIAL"));
    material->addChild(new PropertyTreeItem("Opacity", curve3));
    material->addChild(new PropertyTreeItem("Color R", curve4));

    reset({instance, material});
}

void ExampleCurveModel::currentFrameChangeEmitted(int frame)
{
    qDebug() << "Current frame changed to " << frame;
}

void ExampleCurveModel::curveChangeEmitted(PropertyTreeItem *item)
{
    qDebug() << "The Curve with the id " << item->id() << " has changed";
}

} // End namespace DesignTools.
